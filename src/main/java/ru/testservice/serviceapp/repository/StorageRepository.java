package ru.testservice.serviceapp.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import ru.testservice.serviceapp.model.StorageEntity;

import java.util.List;

public interface StorageRepository extends JpaRepository<StorageEntity, Long> {
    List<StorageEntity> findAllByFolderId(Long folderId);

    void deleteAllByFolderId(Long folderId);
}
