package ru.testservice.serviceapp.web;

import groovy.util.logging.Slf4j;
import org.assertj.core.util.Strings;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.web.PageableDefault;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.thymeleaf.util.StringUtils;
import ru.testservice.serviceapp.dtos.TicketDTO;
import ru.testservice.serviceapp.model.Answer;
import ru.testservice.serviceapp.model.Question;
import ru.testservice.serviceapp.model.Test;
import ru.testservice.serviceapp.service.QuestionService;
import ru.testservice.serviceapp.service.TestService;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.*;
import java.util.stream.Collectors;

@Controller
@RequestMapping("/ticket")
public class TicketController {
    private final Logger logger = LoggerFactory.getLogger(this.getClass());
    private final TestService ts;
    private final QuestionService qs;

    @Autowired
    public TicketController(TestService ts, QuestionService qs) {
        this.ts = ts;
        this.qs = qs;
    }

    @RequestMapping(value = "/ordered", method = RequestMethod.GET)
    public String orderedTicker(@PageableDefault(sort = "order", direction = Sort.Direction.ASC) Pageable pageRequest,
                                @RequestParam Long testId, Model model) {
        Test test = ts.getTest(testId);
        TicketDTO ticketDTO = makeTicketDto(test.getId(), test.getErrorsCount(), qs.getQuestions(test, Collections.emptyList(), pageRequest).getContent());
        int ticketNum = pageRequest.getPageNumber() + 1;
        model.addAttribute("ordered", true);
        int start = 1 + pageRequest.getPageNumber() * pageRequest.getPageSize();
        model.addAttribute("start", start);
        model.addAttribute("end", pageRequest.getPageNumber() * pageRequest.getPageSize() + pageRequest.getPageSize());
        model.addAttribute("ticketDto", ticketDTO);
        model.addAttribute("test", test);
        model.addAttribute("ticketChecked", false);
        model.addAttribute("htmlTitle", makeOrderedTitle(test, pageRequest));
        model.addAttribute("formAction", "/ticket/check?ordered=true&ticketNum=" + ticketNum + "&page=" + pageRequest.getPageNumber());
        model.addAttribute("showProtocol", false);

        return "ticket";
    }

    private String makeOrderedTitle(Test test, Pageable pageRequest) {
        int start = 1 + pageRequest.getPageNumber() * pageRequest.getPageSize();
        int end = pageRequest.getPageNumber() * pageRequest.getPageSize() + pageRequest.getPageSize();
        return String.format("%s | %s | Вопросы %d - %d", test.getSection().getName(), test.getCipher(), start, end);
    }

    @RequestMapping(value = "", method = RequestMethod.GET)
    public String ticket(@RequestParam Long testId, @RequestParam Integer ticketNum,
                         @CookieValue(value = "questions", required = false) String viewedQuestions,
                         @CookieValue(value = "testId", required = false) String testIdSaved,
                         HttpServletResponse response,
                         Model model) {
        Test test = ts.getTest(testId);
        Pageable pg = PageRequest.of(0, test.getQuestionsCount(), Sort.Direction.ASC, "id");

        List<Long> vqIds = viewedQuestions != null ? Arrays.stream(viewedQuestions.split("\\|")).map(Long::parseLong).collect(Collectors.toList()) : Collections.emptyList();
        Long count = qs.countTestQuestions(test.getId());
        if (!testId.toString().equals(testIdSaved)) {
            Cookie testIdCookie = new Cookie("testId", testId.toString());
            testIdCookie.setMaxAge(3600);
            testIdCookie.setPath("/ticket");
            testIdCookie.setHttpOnly(true);
            response.addCookie(testIdCookie);
            vqIds = Collections.emptyList();
            viewedQuestions = "";
        }
        if (count <= vqIds.size()) {
            viewedQuestions = "";
            vqIds = Collections.emptyList();
        }


        TicketDTO ticketDTO = makeTicketDto(test.getId(), test.getErrorsCount(), qs.getQuestions(test, vqIds, pg).getContent());
        StringBuilder sb = new StringBuilder();
        ticketDTO.getQuestionList().forEach(q -> {
            sb.append(q.getId()).append("|");
        });
        if (viewedQuestions != null) {
            sb.append(viewedQuestions);
        } else {
            if (sb.length() > 1)
                sb.deleteCharAt(sb.length() - 1);
        }

        Cookie cookie = new Cookie("questions", sb.toString());
        cookie.setMaxAge(3600);
        cookie.setPath("/ticket");
        cookie.setHttpOnly(true);
        response.addCookie(cookie);
        model.addAttribute("ticketNum", ticketNum);
        model.addAttribute("showProtocol", true);
//        model.addAttribute("allCourses", Collections.singleton(test.getSection().getCourse()));
        model.addAttribute("ticketDto", ticketDTO);
        model.addAttribute("test", test);
        model.addAttribute("start", 1);
        model.addAttribute("ticketChecked", false);
        model.addAttribute("htmlTitle", "Prombez24.ru | Билет №" + ticketNum);
        model.addAttribute("formAction", "/ticket/check?ticketNum=" + ticketNum);

        return "ticket";
    }

    private TicketDTO makeTicketDto(Long testId, Integer errorsCount, List<Question> questions) {
        TicketDTO t = new TicketDTO();
        t.setQuestionList(questions);
        t.setTestId(testId);
        t.setErrorsCount(errorsCount);
        return t;
    }

    @RequestMapping(value = "/check", method = RequestMethod.POST)
    public String checkTicket(@ModelAttribute TicketDTO ticketDto, @RequestParam("ticketNum") Integer ticketNum, Model model,
                              @RequestParam(required = false) Boolean ordered,
                              @RequestParam(defaultValue = "0") Integer page) {
        Collection<Answer> ticketAnswers = getAnswersList(ticketDto.getQuestionList());
        long incorrectCount = 0;
        for (Question question : ticketDto.getQuestionList()) {
            for (Answer answer : question.getAnswers()) {
                if (answer.isCorrect() != answer.isChecked()) {
                    incorrectCount++;
                    break;
                }
            }
        }
        long correctCount = ticketAnswers.stream().filter(Answer::isCorrect).count();
        long correctCheckedCount = ticketAnswers.stream().filter(a -> a.isCorrect() && a.isChecked()).count();
        model.addAttribute("checkedCorrect", correctCheckedCount);
        model.addAttribute("corrects", correctCount);
        model.addAttribute("page", page);
        model.addAttribute("ticketNum", ticketNum);
        model.addAttribute("incorrectCount", incorrectCount);
        model.addAttribute("ticketDto", ticketDto);
        Test test = ts.getTest(ticketDto.getTestId());
        model.addAttribute("test", test);
        model.addAttribute("ticketChecked", true);
        model.addAttribute("htmlTitle", " Билет №" + ticketNum);
        model.addAttribute("protocol", ticketDto.isFormProtocol());
        model.addAttribute("formAction", ticketDto.isFormProtocol() ? "/protocol" : "");
        model.addAttribute("start", 1 + page * test.getOrderedQuestionsCount());
        model.addAttribute("end",  page * test.getOrderedQuestionsCount() + test.getOrderedQuestionsCount());
        model.addAttribute("ordered", ordered);
        ticketDto.setSubject(String.format("%s %s", test.getCode(), test.getTitle()));
        return "ticket";
    }


    private Collection<Answer> getAnswersList(List<Question> questions) {
        List<Answer> answers = new ArrayList<>();
        questions.forEach(q -> {
            answers.addAll(q.getAnswers());
        });
        return answers;
    }

}
