package ru.testservice.serviceapp.service;

import org.apache.poi.openxml4j.opc.OPCPackage;
import org.apache.poi.xwpf.usermodel.*;
import org.openxmlformats.schemas.wordprocessingml.x2006.main.CTPicture;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.multipart.MultipartFile;
import ru.testservice.serviceapp.model.Answer;
import ru.testservice.serviceapp.model.Question;
import ru.testservice.serviceapp.model.StorageEntity;
import ru.testservice.serviceapp.model.Test;
import ru.testservice.serviceapp.repository.AnswerRepository;
import ru.testservice.serviceapp.repository.QuestionRepository;
import ru.testservice.serviceapp.repository.TestRepository;

import javax.validation.constraints.NotNull;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Optional;

@Service
public class QuestionService {
    private final Logger log = LoggerFactory.getLogger(this.getClass());
    private final QuestionRepository repository;
    private final AnswerRepository answerRepository;
    private final TestRepository tr;
    private Question buferQuestion;
    private Test testLink;
    private final IStorageService storageService;

    @Autowired
    public QuestionService(QuestionRepository repository, AnswerRepository answerRepository, TestRepository tr,
                           IStorageService storageService) {
        this.repository = repository;
        this.answerRepository = answerRepository;
        this.tr = tr;
        this.storageService = storageService;
    }

    public QuestionService with(Test test) {
        this.testLink = test;
        return this;
    }

    public Question save(Question question) {
        boolean isNew = question.getId() == null;
        question = repository.save(question);
        if (isNew) {
            question.getTest().increaseQN();
            tr.save(question.getTest());
        }
        return question;
    }

    public List<Question> getQuestions(List<Long> questionIds) {
        return (List<Question>) repository.findAllById(questionIds);
    }

    private void extract(XWPFParagraph paragraph, List<Question> questions,
                         boolean isQuestionText, Optional<XWPFRun> imageParagraph) {
        String text = paragraph.getText().trim();
        if (!imageParagraph.isPresent() && text.length() < 3) return;

        if (isQuestionText) {
            // Текст вопроса - нужно создавать новый вопрос
            if (text.indexOf(".") <= 3 && text.indexOf(".") > 0) {
                text = text.substring(text.indexOf(".") + 1);
            }
            if (buferQuestion != null) {
                if (!buferQuestion.getAnswers().isEmpty()) {
                    questions.add(buferQuestion.clone());
                    log.debug("{}", buferQuestion.toString());
                }
            }
            buferQuestion = new Question(text.trim(), new ArrayList<>(), testLink);
            final Optional<XWPFRun> imageInQuestionParagraph = getImageRunInParagraph(paragraph);
            if (imageInQuestionParagraph.isPresent()) {
                addImageToQuestion(imageInQuestionParagraph);
            }
        } else if (imageParagraph.isPresent()) {
            addImageToQuestion(imageParagraph);
        } else {
            // Текст ответа - если создан новый вопрос - добавляем ответ, если нет - прерываемся
            if (text.charAt(1) == ')') text = text.substring(2);
            if (buferQuestion == null) return;
            Optional<XWPFRun> first = paragraph.getRuns().stream().filter(run -> run.getColor() != null).findFirst();
            if (first.isPresent()) {
                String color = first.get().getColor();
                if (color.equals("FF0000")) {
                    buferQuestion.setNtdLink(text);
                    return;
                }
            }
            XWPFRun isItalic = paragraph.getRuns().stream().filter(XWPFRun::isItalic).findFirst().orElse(null);
            Answer ans = new Answer(text.trim(), null != isItalic, buferQuestion);
            buferQuestion.getAnswers().add(ans);
        }
    }

    private void addImageToQuestion(Optional<XWPFRun> imageRunInParagraph) {
        if (buferQuestion == null) return;
        imageRunInParagraph.ifPresent(xwpfRun -> {
            List<XWPFPicture> embeddedPictures = xwpfRun.getEmbeddedPictures();
            CTPicture ctPicture = hasCTPicts(xwpfRun) ? xwpfRun.getCTR().getPictArray()[0] : null;
            if (embeddedPictures != null && !embeddedPictures.isEmpty()) {
                final XWPFPicture xwpfPicture = embeddedPictures.get(0);
                try {
                    final StorageEntity entity = storageService.store(FileUtils.fromXWPFPicture(xwpfPicture), "img/");
                    buferQuestion.setFile(entity);
                } catch (IOException e) {
                    log.error("Error with picture storing", e);
                }
            } else if (ctPicture != null) {
                log.info("CTPicture not null");
                try {
                    final StorageEntity entity = storageService.store(FileUtils.fromCTPicture(getCtImageId(xwpfRun.getCTR().getPictArray(0).xmlText()),
                            xwpfRun.getDocument()), "img/");
                    buferQuestion.setFile(entity);
                } catch (IOException e) {
                    log.error("Error with picture storing", e);
                }
            }
        });
    }

    public String getCtImageId(String xml) {
        int i = xml.indexOf("r:id=\"") + 5;
        char buf = xml.charAt(++i);
        StringBuilder sb = new StringBuilder();
        while (buf != '\"') {
            sb.append(buf);
            buf = xml.charAt(++i);
        }
        return sb.toString();
    }

    private boolean hasCTPicts(XWPFRun xwpfRun) {
        return xwpfRun.getCTR().getPictArray() != null && xwpfRun.getCTR().getPictArray().length > 0;
    }

    private void parseTables(MultipartFile file, List<Question> questions) {
        try {
            XWPFDocument xdoc = new XWPFDocument(OPCPackage.open(file.getInputStream()));
            Iterator bodyElementIterator = xdoc.getBodyElementsIterator();
            while (bodyElementIterator.hasNext()) {
                IBodyElement element = (IBodyElement) bodyElementIterator.next();
                if ("TABLE".equalsIgnoreCase(element.getElementType().name())) {
                    List<XWPFTable> tableList = element.getBody().getTables();
                    for (XWPFTable table : tableList) {
                        for (int i = 0; i < table.getRows().size(); i++) {
                            XWPFTableRow row = table.getRow(i);
                            XWPFTableCell cell = row.getCell(0);
                            XWPFParagraph paragraph = cell.getParagraphArray(0);
                            String numFmt = paragraph.getNumFmt();
                            final Optional<XWPFRun> imageRunInParagraph = getImageRunInParagraph(paragraph);
                            extract(paragraph, questions, numFmt != null && numFmt.equalsIgnoreCase("decimal"), imageRunInParagraph);
                        }
                    }
                }
            }
            questions.add(this.buferQuestion.clone());
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    private Optional<XWPFRun> getImageRunInParagraph(XWPFParagraph paragraph) {
        return paragraph.getRuns().stream()
                .filter(run -> run.getEmbeddedPictures() != null && !run.getEmbeddedPictures().isEmpty()
                        || hasCTPicts(run)).findFirst();
    }

    private void parseByLists(MultipartFile file, List<Question> questions) {
        try {
            XWPFDocument xdoc = new XWPFDocument(OPCPackage.open(file.getInputStream()));
            xdoc.getParagraphs().forEach((XWPFParagraph paragraph) -> {
                final Optional<XWPFRun> imageRunInParagraph = getImageRunInParagraph(paragraph);
                extract(paragraph, questions, isBold(paragraph) || paragraph.getText().contains("?"), imageRunInParagraph);
            });
            questions.add(this.buferQuestion.clone());
        } catch (Exception ex) {
            log.error("ParseByLists exception. File name: {}", file.getOriginalFilename(), ex);
        }

    }

    private boolean isBold(XWPFParagraph paragraph) {
        return paragraph.getRuns().stream().anyMatch(XWPFRun::isBold);
    }

    @Transactional
    public void uploadQuestions(List<MultipartFile> files) {
        List<Question> questions = new ArrayList<>();
        files.forEach(f -> {
            boolean tabled = false;
            if (f.getOriginalFilename() == null) {
                log.error("File has null original name. Will try simple strategy");
            } else {
                tabled = f.getOriginalFilename().contains("__tabled");
                log.debug("File {} {} tabled ", f.getOriginalFilename(), tabled ? "is" : "is not");
            }
            if (tabled) {
                parseTables(f, questions);
            } else {
                parseByLists(f, questions);
            }
        });
        int size = questions.size();
        log.info("Parsed questions: {}", size);
        testLink.setQuestionsNumber(testLink.getQuestionsNumber() + size);
        for (int i = 0; i < questions.size(); i++) {
            questions.get(i).setOrder(i);
        }
        repository.saveAll(questions);
        tr.save(testLink);
        testLink = null;
    }

    public Page<Question> getQuestions(@NotNull Long testId, @NotNull Pageable pageable) {
        return repository.findAllByTestId(testId, pageable);
    }

    public Page<Question> getQuestions(@NotNull Test test, List<Long> except, @NotNull Pageable pageable) {
        if (except == null || except.size() == 0) {
            return repository.findAllByTestId(test.getId(), pageable);
        }
        return repository.findAllByTestIdExcpet(test, except, pageable);
    }


    public Long countTestQuestions(Long id) {
        return repository.countQuestionsByTestId(id);
    }

    public void deleteById(Long id) {
        Optional<Question> optional = repository.findById(id);
        optional.ifPresent((one) -> {
            if (one.getFile() != null) {
                storageService.deleteFile(one.getFile());
            }
            repository.deleteById(id);
        });

    }

    @Transactional
    public void deleteByTestId(@NotNull Long id) {
        Pageable pageable = PageRequest.of(0, 50);
        Page<Question> questions = repository.findAllByTestId(id, pageable);
        do{
            questions.forEach(q -> {
                if(q.getFile() != null){
                    storageService.deleteFile(q.getFile());
                }
            });
            repository.deleteAll(questions.getContent());
            questions = repository.findAllByTestId(id, pageable);
        } while (!questions.isLast());
    }

    @Transactional
    public Answer saveAnswer(Answer answer) {
        return answerRepository.save(answer);
    }

    public Question getQuestion(Long id) {
        return repository.findById(id).get();
    }

    public void deleteAnswerById(Long id) {
        answerRepository.deleteById(id);
    }

    public Answer getAnswer(Long id) {
        return answerRepository.findById(id).get();
    }
}
