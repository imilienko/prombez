package ru.testservice.serviceapp.service;

import org.apache.poi.xwpf.usermodel.Document;
import org.apache.poi.xwpf.usermodel.XWPFDocument;
import org.apache.poi.xwpf.usermodel.XWPFPicture;
import org.apache.poi.xwpf.usermodel.XWPFPictureData;
import org.springframework.http.MediaType;
import org.springframework.web.multipart.MultipartFile;

import java.io.*;

public class FileUtils {
    public static MultipartFile fromXWPFPicture(XWPFPicture picture){
        return new ByteMultipartFile(picture.getPictureData().getData(), picture.getPictureData().getFileName(), getContentType(picture.getPictureData().getPictureType()));
    }

    private static String getContentType(int pictureType) {
        switch (pictureType){
            case Document.PICTURE_TYPE_PNG: return MediaType.IMAGE_PNG_VALUE;
            case Document.PICTURE_TYPE_JPEG:
            default: return MediaType.IMAGE_JPEG_VALUE;
        }
    }

    public static MultipartFile fromCTPicture(String ctPictureId, XWPFDocument document) {
        XWPFPictureData pictureDataByID = document.getPictureDataByID(ctPictureId);
        return new ByteMultipartFile(pictureDataByID.getData(), pictureDataByID.getFileName(), getContentType(pictureDataByID.getPictureType()));

    }

    private static class ByteMultipartFile implements MultipartFile, Serializable {
        private byte[] data;
        private String name;
        private String contentType;
        ByteMultipartFile(byte[] data, String name, String contentType) {
            this.data = data;
            this.name = name;
            this.contentType = contentType;
        }

        @Override
        public String getName() {
            return name;
        }

        @Override
        public String getOriginalFilename() {
            return name;
        }

        @Override
        public String getContentType() {
            return contentType;
        }

        @Override
        public boolean isEmpty() {
            return data == null || data.length == 0;
        }

        @Override
        public long getSize() {
            return data.length;
        }

        @Override
        public byte[] getBytes() throws IOException {
            return data;
        }

        @Override
        public InputStream getInputStream() throws IOException {
            return new ByteArrayInputStream(this.data);
        }

        @Override
        public void transferTo(File dest) throws IOException, IllegalStateException {
            final FileOutputStream fileOutputStream = new FileOutputStream(dest);
            fileOutputStream.write(this.data);
        }
    }
}
